import websocket
import serial
import threading
import sys

ws_host = sys.argv[1]
ws_port = sys.argv[2]
ser_port = sys.argv[3]

ser = serial.Serial(port=ser_port, baudrate=115200, bytesize=serial.SEVENBITS, parity=serial.PARITY_EVEN, timeout=2.0)

def on_message(ws, message):
    print("Data received\t:", message)
    ser.write(message.encode())

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    print("### Connected to the server ###")
    thread1 = Sensor(1, 'thread1', ser, ws)
    thread1.start()

class Sensor(threading.Thread):
    def __init__(self, threadID, name, *args):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.args = args

    def run(self):
        while True:
            
            data = self.args[0].readline()
            if len(data)>=1:
                print("Data sent\t:", data)
                self.args[1].send(data)

if __name__ == "__main__":
##    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("ws://" + ws_host + ":" + str(ws_port) + "/",
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
    ws.on_open = on_open
    ws.run_forever()
