from websocket_server import WebsocketServer
import sys

# Called for every client connecting (after handshake)
def new_client(client, server):
        print("New client connected and was given id %d" % client['id'])
        print (WebsocketServer.clients)

# Called for every client disconnecting
def client_left(client, server):
        print("Client(%d) disconnected" % client['id'])

# Called when a client sends a message
def message_received(client, server, message):
        print("Client(%d) said: %s" % (client['id'], message))
        if client['id'] == 1:
                server.send_message(WebsocketServer.clients[1], message)
        elif client['id'] == 2:
                server.send_message(WebsocketServer.clients[0], message)
                
HOST=sys.argv[1]           
PORT=int(sys.argv[2])
server = WebsocketServer(HOST, PORT)
server.set_fn_new_client(new_client)
server.set_fn_client_left(client_left)
server.set_fn_message_received(message_received)
server.run_forever()
