# PLC-Software communication over Websockets - User Guide
Generally a PLC is programed through a physical wire. In this project the same 
communication in between the PLC & PLC programing software is sent through a
websocket server-client model. The following is the architecture of the project.

![](/assets/images/Archi.png)

## Requirements:
*  PLC - [SR3 B101BD](https://www.schneider-electric.com/en/product/SR3B101BD/modular-smart-relay-zelio-logic---10-i-o---24-v-dc---clock---display/)
*  Serial connector - [SR2 USB01](https://www.schneider-electric.com/en/product/SR2USB01/usb-pc-connecting-cable---for-smart-relay-zelio-logic---3-m/)

### [Downloading the project](#downloading-the-project-1)
### [Running the server](#running-the-server-1)
### [Setting up Device 1](#setting-up-device-1-1)
### [Setting up Device 2](#setting-up-device-2-1)

## Downloading the project

1. Download or clone the project from the [repository](https://gitlab.com/MFMMuaaz/plc-raspberry-pi-communication.git).

## Running the server

1.  [Download](https://www.python.org/downloads/) and install python 3.
2.  Run the python script [server.py](/server/server.py) with the parameters 
    host and port. 

    **For example**:

    ``` bash
    python3 server.py 192.168.8.104 9001
    ```

## Setting up Device 1
**Prerequisite**:
Windows operating system

1.  [Download](https://www.schneider-electric.com/en/download/document/ZelioSoft2_V5_3/) 
    and install the PLC programing software Zelio Soft 2.
2.  [Download](https://www.virtual-serial-port.org/vspd-post-download.html) and 
    install the Virtual COM port driver.
3.  Launch Virtual COM port driver and click on "continue Demo".

    ![](/assets/images/VSP-1.png)

4.  Select two unused port names and click on "Add pair".

    ![](/assets/images/VSP-2.png)

    **Note**: Now a virtual port should be created on device 1. You can assure by 
    checking the ports on Device Manager.

    ![](/assets/images/Device_manager.png)

5.  Install python libraries pyserial and websocket-client. You can install them 
    by running following commands on command line.
    
    ```bash
    pip install pyserial
    ```
    
    ```bash
    pip install websocket-client
    ```

6.  Copy and run the python script [Software-agent.py](/agents/Software-agent.py) on device 1. Software-agent.py
    has the following signature.
    
    ```bash
    python3 Software-agent.py <server-ip> <websocket port> <serial port>
    ```
    
    **Parameters**:

    | Parameter | Description |
    | :------ | :------ |
    | server-ip | IP address of the web socket server |
    | websocket port | The opened port to connect to the web socket | 
    | serial port | One terminal of the virtual port created in [3] |
    
    **For example**:
    ```bash
    python3 Software-agent.py 192.168.8.104 9001 COM1
    ```
    
## Setting up Device 2
1.  [Download](https://download.schneider-electric.com/files?p_enDocType=Software+-+Released&p_File_Name=SR2_USB01_H2.x_Setup.exe&p_Doc_Ref=DriverSR2USB01_H2) and install the driver for SR2 USB01.
2.  Connect the PLC with SR2 USB01 serial cable to the device 2.
3.  [Download](https://www.python.org/downloads/) and install python 3 on device 2.
4.  Install python libraries pyserial and websocket-client. You can install them 
    by running following commands on command line.
    
    ```bash
    pip install pyserial
    ```
    
    ```bash
    pip install websocket-client
    ```

5.  Copy and run the python script [PLC-agent.py](agents/PLC-agent.py) on device 2. PLC-agent.py 
    has the following signature.
    
    ```bash
    python3 PLC-agent.py <server-ip> <websocket port> <serial port>
    ```  
    
    **Parameters**:
    
    | Parameter | Description |
    | :------ | :------ |
    | server-ip | IP address of the web socket server |
    | websocket port | The opened port to connect to the web socket | 
    | serial port | The physical serial port which is physically connected with PLC |
    
    **For example**:
    ```bash
    python3 PLC-agent.py 192.168.8.104 9001 COM5
    ```

## ###

After the above processes the system is ready to be used. Launch the software 
Zelio Soft 2 on device 1. Then go to *Transfer > COMMUNICATION configuration* it 
will pop up the following window.

![](/assets/images/ZelioSoft.png)

Select the "Com Port" radio button and select the remaining terminal of the 
virtul serial port created [above](#setting-up-device-1-1). Then click on "Test"
and it will prompt "Connection successful" message. Then click ok and try to write 
a program and upload it to PLC and read an existing program on PLC.